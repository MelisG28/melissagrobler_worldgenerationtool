﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class SelectChangeCells : MonoBehaviour
{
    [SerializeField] GridGen grid;
    new Camera camera;
    bool changeToDead = false;

    void Awake ()
    {
        camera = Camera.main;
    }

    void OnGUI ()
    {
        if (Event.current.type == EventType.MouseDown)//Check if cell is being selected and turn it on or off depending on its current state
        {
            // Debug.Log ("Select Cell");
            SelectingCells (true);
        }

        if(Event.current.type == EventType.MouseDrag)
        {
            SelectingMultipleCells(true);
        }
    }

    void SelectingCells (bool selecting) //Function for when placing objects
    {
        //Get the location of the mouse click
        Vector3 mousePos = Event.current.mousePosition;
        float _pixPerPoint = EditorGUIUtility.pixelsPerPoint;
        mousePos.y = camera.pixelHeight - mousePos.y * _pixPerPoint;
        mousePos.x *= _pixPerPoint;

        Vector3 worldPoint = camera.ScreenToWorldPoint (mousePos);

        worldPoint = grid.transform.InverseTransformPoint (worldPoint);
        worldPoint = new Vector3 (Mathf.Round (worldPoint.x), Mathf.Round (worldPoint.y));

        if (worldPoint.x > grid.gridSize.x - 1 || worldPoint.x < 0) return;
        if (worldPoint.y > grid.gridSize.y - 1 || worldPoint.y < 0) return;

        int childIndex = (int) worldPoint.x + ((int) worldPoint.y * grid.gridSize.y);

        if (selecting)
        {
            changeToDead = grid.cells[childIndex].alive;
        }

        grid.cells[childIndex].Set (!changeToDead);
    }

    void SelectingMultipleCells (bool selecting) //Function for when drawing the floor
    {
        //Get the location of the mouse click
        Vector3 mousePos = Event.current.mousePosition;
        float _pixPerPoint = EditorGUIUtility.pixelsPerPoint;
        mousePos.y = camera.pixelHeight - mousePos.y * _pixPerPoint;
        mousePos.x *= _pixPerPoint;

        Vector3 worldPoint = camera.ScreenToWorldPoint (mousePos);

        worldPoint = grid.transform.InverseTransformPoint (worldPoint);
        worldPoint = new Vector3 (Mathf.Round (worldPoint.x), Mathf.Round (worldPoint.y));

        if (worldPoint.x > grid.gridSize.x - 1 || worldPoint.x < 0) return;
        if (worldPoint.y > grid.gridSize.y - 1 || worldPoint.y < 0) return;

        int childIndex = (int) worldPoint.x + ((int) worldPoint.y * grid.gridSize.y);

        if (selecting)
        {
            changeToDead = grid.cells[childIndex].alive;
        }

        grid.cells[childIndex].SetMultiple (!changeToDead);
    }
}
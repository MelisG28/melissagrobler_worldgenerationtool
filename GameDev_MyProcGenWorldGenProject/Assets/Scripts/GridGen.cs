﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class GridGen : MonoBehaviour
{
    [HideInInspector][SerializeField] GameObject cellPrefab;
    [HideInInspector][SerializeField] Transform cubeParent;
    [HideInInspector][SerializeField] GameObject floor;
    [HideInInspector][SerializeField] GameObject meshFloor;
    [HideInInspector][SerializeField] GameObject meshFloorBlockPrefab;
    [HideInInspector][SerializeField] MeshFilter[] floorMeshes;
    [HideInInspector] public List<GameObject> listOfMeshes = new List<GameObject> ();

    [Header ("Settings")]
    public Vector2Int gridSize;
    [HideInInspector] public GridCell[] cells;
    [HideInInspector] public Mesh _mesh;
    [HideInInspector] public List<GameObject> listOfCubes = new List<GameObject> ();

    // [Header ("Brushes")]
    // [SerializeField] GridBrush _lineBrush;
    // [SerializeField] GridBrush _squareBrush;
    // [SerializeField] GridBrush _triangleBrush;
    // [SerializeField] GridBrush _circleBrush;
    // [SerializeField] Sprite _line;
    // [SerializeField] List<Color> _colourLine = new List<Color> { Color.red, Color.blue, Color.green };
    // [SerializeField] Sprite _square;
    // [SerializeField] List<Color> _colourSquare = new List<Color> { Color.red, Color.blue, Color.green };
    // [SerializeField] Sprite _triangle;
    // [SerializeField] List<Color> _colourTriangle = new List<Color> { Color.red, Color.blue, Color.green };
    // [SerializeField] Sprite _circle;
    // [SerializeField] List<Color> _colourCircle = new List<Color> { Color.red, Color.blue, Color.green };

    [Header ("Prefabs")]
    [SerializeField] GameObject redCubePrefab;
    [SerializeField] GameObject blueCubePrefab;
    [SerializeField] GameObject greenCubePrefab;

    public void GenerateGrid () //UI
    {
        int desiredCells = gridSize.x * gridSize.y;

        if (desiredCells < transform.childCount) //Destroy excess children
        {
            int difference = transform.childCount - desiredCells;

            for (int i = 0; i < difference; i++)
            {
                DestroyImmediate (transform.GetChild (0).gameObject);
            }
        }
        else if (desiredCells > transform.childCount) //Add more children
        {
            int difference = desiredCells - transform.childCount;

            for (int i = 0; i < difference; i++)
            {
#if UNITY_EDITOR
                PrefabUtility.InstantiatePrefab (cellPrefab, transform);
#else
                Instantiate (cellPrefab, transform);
#endif
            }
        }

        ArrangeCells ();
    }

    void ArrangeCells ()
    {
        cells = new GridCell[transform.childCount];

        Vector2Int coordinate = Vector2Int.zero;

        for (int i = 0; i < transform.childCount; i++)
        {
            GameObject cell = transform.GetChild (i).gameObject;
            cell.name = "Cell " + coordinate;

            cell.transform.localPosition = new Vector3 (coordinate.x, coordinate.y);

            cells[i] = cell.GetComponent<GridCell> ();
            cells[i].coordinate = coordinate;

            coordinate.x++;
            if (coordinate.x >= gridSize.x)
            {
                coordinate.y++;
                coordinate.x = 0;
            }
        }

        CenterCamera ();
    }

    void CenterCamera ()
    {
        transform.position = new Vector3 (-gridSize.x / 2, -gridSize.y / 2);
        transform.position += new Vector3 (gridSize.x % 2 == 0 ? 0.5f : 0, gridSize.y % 2 == 0 ? 0.5f : 0);

        Camera.main.orthographicSize = (gridSize.x > gridSize.y ? gridSize.x : gridSize.y) / 1.8f;
    }

    public void GenerateMesh ()
    {
        meshFloor.GetComponent<MeshFilter> ().sharedMesh.Clear ();
        meshFloor.transform.position = new Vector3 (0, 0, 0);
        meshFloor.transform.localScale = new Vector3 (1, 1, 1);

        GenerateFloorMeshes ();
        CombineFloorMeshes ();
    }

    public void GenerateFloorMeshes ()
    {
        for (int i = 0; i < cells.Length; i++)
        {
            if (cells[i].alive)
            {
                // Debug.Log ("Spawning floor mesh block");
                GameObject meshBlockPrefab = meshFloorBlockPrefab;
                Vector2Int _coordinate = cells[i].coordinate;
                Vector3 meshBlockPos = new Vector3 (_coordinate.x, 0f, _coordinate.y);
                GameObject floorMeshBlock = Instantiate (meshBlockPrefab, meshBlockPos, meshBlockPrefab.transform.rotation);
                floorMeshBlock.transform.parent = meshFloor.transform;
                listOfMeshes.Add (floorMeshBlock);
            }
        }
    }

    public void CombineFloorMeshes ()
    {
        floorMeshes = meshFloor.GetComponentsInChildren<MeshFilter> ();
        
        _mesh = new Mesh ();

        CombineInstance[] combinedMeshes = new CombineInstance[floorMeshes.Length];

        for (int i = 0; i < floorMeshes.Length; i++)
        {
            if (floorMeshes[i].transform == meshFloor.transform)
            {
                continue;
            }

            combinedMeshes[i].subMeshIndex = 0;
            combinedMeshes[i].mesh = floorMeshes[i].sharedMesh;
            combinedMeshes[i].transform = floorMeshes[i].transform.localToWorldMatrix;
        }

        _mesh.CombineMeshes (combinedMeshes);
        meshFloor.GetComponent<MeshFilter> ().sharedMesh = _mesh;

        for (int j = 0; j < meshFloor.transform.childCount; j++)
        {
            meshFloor.transform.GetChild (j).gameObject.SetActive (false);
        }
    }

    public void GenerateCubes ()
    {
        for (int i = 0; i < cells.Length; i++)
        {
            if (cells[i].alive)
            {
                // Debug.Log ("Spawning cube");
                GameObject cubePrefab = GetCorrectPrefab (cells[i]);
                float cubeYPos = cubePrefab.transform.localScale.y / 2;
                Vector2Int _coordinate = cells[i].coordinate;
                Vector3 cubePos = new Vector3 (_coordinate.x, cubeYPos, _coordinate.y);
                GameObject cube = Instantiate (cubePrefab, cubePos, transform.rotation);
                cube.transform.parent = cubeParent;
                listOfCubes.Add (cube);
            }
        }
    }

    public GameObject GetCorrectPrefab (GridCell cell)
    {
        GameObject prefab = null;

        if (cell._currentColour == Color.red) prefab = redCubePrefab;
        if (cell._currentColour == Color.blue) prefab = blueCubePrefab;
        if (cell._currentColour == Color.green) prefab = greenCubePrefab;
        // Debug.Log ("Prefab: " + prefab);
        return prefab;
    }

    public void FloorMeshCompleted ()
    {
        foreach (var cell in cells) //Remove dead cells that aren't part of the floor mesh
        {
            if(!cell.alive)
            {
                cell.deactivate = true;
                cell.gameObject.SetActive(false);
            }
            else
            {
                cell.ResetCells();
            }
        }
    }

    public void ClearCells ()
    {
        // Debug.Log ("Clearing Cells");

        foreach (var cell in cells)
        {
            if(!cell.deactivate)
            {
                cell.ResetCells ();
            }
        }
    }

    public void DeleteCells ()
    {
        // Debug.Log ("Deleting Cells");

        foreach (var cell in cells)
        {
            DestroyImmediate (cell.gameObject);
        }

        cells = new GridCell[0];
    }

    public void DeleteCubes ()
    {
        // Debug.Log ("Deleting Cubes");

        foreach (var cube in listOfCubes)
        {
            DestroyImmediate (cube.gameObject);
        }

        listOfCubes.Clear ();
    }

    public void DeleteMesh ()
    {
        for (int j = 0; j < listOfMeshes.Count; j++)
        {
            DestroyImmediate(listOfMeshes[j].gameObject);
        }

        meshFloor.GetComponent<MeshFilter> ().sharedMesh.Clear ();
        meshFloor.transform.position = new Vector3 (0, 0, 0);
        meshFloor.transform.localScale = new Vector3 (1, 1, 1);
    }
}
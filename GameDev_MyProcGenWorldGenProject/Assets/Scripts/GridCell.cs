﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridCell : MonoBehaviour
{
    public Vector2Int coordinate;
    [SerializeField] SpriteRenderer spriteRenderer;
    public List<Color> _colours = new List<Color> ();
    public Color colourDead = Color.gray;
    public bool alive = false;
    public bool deactivate = false;
    public Color _currentColour;
    public int _colIndex = 0;

    public void Flip (bool cellLive)
    {
        if (cellLive)
        {
            alive = true;
            Color _newColour = GetNewColour ();
            spriteRenderer.color = _newColour;
            _currentColour = _newColour;
        }
        else if (!cellLive)
        {
            // Debug.Log ("Cell died");
            alive = false;
            spriteRenderer.color = colourDead;
            _currentColour = colourDead;
        }
    }

    public void FlipMultiple (bool cellLive)
    {
        if (cellLive)
        {
            alive = true;
            Color _newColour = _colours[0];
            spriteRenderer.color = _newColour;
            _currentColour = _newColour;
        }
        else if (!cellLive)
        {
            // Debug.Log ("Cell died");
            alive = false;
            spriteRenderer.color = colourDead;
            _currentColour = colourDead;
        }
    }

    public Color GetNewColour ()
    {
        Color _colour = Color.white;

        if (spriteRenderer.color == colourDead)
        {
            _colour = Color.red;
            // Debug.Log ("Red");
        }
        else if (spriteRenderer.color == Color.red)
        {
            _colour = Color.blue;
            // Debug.Log ("Blue");
        }
        else if (spriteRenderer.color == Color.blue)
        {
            _colour = Color.green;
            // Debug.Log ("Green");
        }

        return _colour;
    }

    public void Set (bool _alive)
    {
        if (!alive && _alive)
        {
            // Debug.Log ("Be alive");
            Flip (true); //if our cell is dead, make it alive
        }
        else if (alive && !_alive && spriteRenderer.color != _colours[2])
        {
            // Debug.Log ("More Colours");
            Flip (true); //more colours left to try, the cell still lives
        }
        else if (alive && !_alive && spriteRenderer.color == _colours[2])
        {
            // Debug.Log ("Be dead");
            Flip (false); //the next colour is dead, make it dead
        }
        else if (alive && !_alive || !alive && !_alive)
        {
            // Debug.Log ("Be dead");
            Flip (false); //the next colour is dead, make it dead
        }
    }

    public void SetMultiple (bool _alive)
    {
        if(!alive && _alive)
        {
            FlipMultiple (true);
        }
        else
        {
            FlipMultiple (false);
        }
    }

    public void ResetCells ()
    {
        alive = false;
        spriteRenderer.color = colourDead;
        _currentColour = colourDead;
    }
}
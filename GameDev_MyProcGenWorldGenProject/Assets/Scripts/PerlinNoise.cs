﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerlinNoise : MonoBehaviour
{
    public float power = 3.0f;
    public float scale = 1.0f;

    public void MakeSomeNoise ()
    {
        MeshFilter meshFilter = GetComponent<MeshFilter> ();
        Vector3[] vertices = meshFilter.sharedMesh.vertices;

        for (int i = 0; i < vertices.Length; i++)
        {
            float xCoord = vertices[i].x * scale;
            float yCoord = vertices[i].z * scale;
            vertices[i].y = (Mathf.PerlinNoise (xCoord, yCoord) - 0.5f) * power;
        }

        meshFilter.sharedMesh.vertices = vertices;
        meshFilter.sharedMesh.RecalculateBounds ();
        meshFilter.sharedMesh.RecalculateNormals ();
    }
}
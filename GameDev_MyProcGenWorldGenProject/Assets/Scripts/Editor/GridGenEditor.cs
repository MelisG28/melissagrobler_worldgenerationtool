﻿using UnityEditor;
using UnityEngine;

[CustomEditor (typeof (GridGen))]
public class GridGenEditor : Editor
{
    SerializedProperty lineBrushProp;
    SerializedProperty squareBrushProp;
    SerializedProperty triangleBrushProp;
    SerializedProperty circleBrushProp;
    // SerializedProperty lineSpriteProp;
    // SerializedObject lineSpriteObj;
    // SerializedProperty squareSpriteProp;
    // SerializedProperty triangleSpriteProp;
    // SerializedProperty circleSpriteProp;
    // // Sprite lineSprite;
    // // Texture2D lineTexture;
    // void OnEnable ()
    // {
    //     lineSpriteProp = serializedObject.FindProperty ("_line");
    //     squareSpriteProp = serializedObject.FindProperty ("_square");
    //     triangleSpriteProp = serializedObject.FindProperty ("_triangle");
    //     circleSpriteProp = serializedObject.FindProperty ("_circle");
    //     // lineSprite = lineSpriteProp.objectReferenceValue as Sprite;
    //     // lineTexture = lineSprite.texture;
    // }
    void OnEnable ()
    {
        lineBrushProp = serializedObject.FindProperty("_lineBrush");
        squareBrushProp = serializedObject.FindProperty("_squareBrush");
        triangleBrushProp = serializedObject.FindProperty("_triangleBrush");
        circleBrushProp = serializedObject.FindProperty("_circleBrush");
    }

    public override void OnInspectorGUI ()
    {
        serializedObject.Update ();
        base.OnInspectorGUI ();

        // EditorGUILayout.LabelField("Brushes");
        // EditorGUILayout.PropertyField(lineBrushProp, GUIContent.none, GUILayout.Height (0));
        // EditorGUILayout.Space (50);
        // EditorGUILayout.PropertyField(squareBrushProp, GUIContent.none, GUILayout.Height (0));
        // EditorGUILayout.Space (50);
        // EditorGUILayout.PropertyField(triangleBrushProp, GUIContent.none, GUILayout.Height (0));
        // EditorGUILayout.Space (50);
        // EditorGUILayout.PropertyField(circleBrushProp, GUIContent.none, GUILayout.Height (0));

        EditorGUILayout.Space (25);
        EditorGUILayout.LabelField ("Generate");

        if (GUILayout.Button ("Generate Drawing Grid"))
        {
            ((GridGen) target).GenerateGrid ();
        }

        if (GUILayout.Button ("Generate Floor Mesh"))
        {
            ((GridGen) target).GenerateMesh ();
        }

        if (GUILayout.Button ("Generate Prefabs"))
        {
            ((GridGen) target).GenerateCubes ();
        }

        EditorGUILayout.Space (15);
        EditorGUILayout.LabelField ("Complete");

        if (GUILayout.Button ("Floor Mesh Completed"))
        {
            ((GridGen) target).FloorMeshCompleted ();
        }

        EditorGUILayout.Space (15);
        EditorGUILayout.LabelField ("Clear");

        if (GUILayout.Button ("Clear Painted Cells"))
        {
            ((GridGen) target).ClearCells ();
        }

        EditorGUILayout.Space (15);
        EditorGUILayout.LabelField ("Delete");

        if (GUILayout.Button ("Delete Floor Mesh"))
        {
            ((GridGen) target).DeleteMesh ();
        }

        if (GUILayout.Button ("Delete Drawing Grid"))
        {
            ((GridGen) target).DeleteCells ();
        }

        if (GUILayout.Button ("Delete Prefabs"))
        {
            ((GridGen) target).DeleteCubes ();
        }

        EditorGUILayout.Space ();
        serializedObject.ApplyModifiedProperties ();
    }
}
﻿using UnityEditor;
using UnityEngine;

[CustomEditor (typeof (PerlinNoise))]
public class PerlinNoiseEditor : Editor
{
    public override void OnInspectorGUI ()
    {
        serializedObject.Update ();
        base.OnInspectorGUI ();

        if (GUILayout.Button ("Add Noise"))
        {
            ((PerlinNoise) target).MakeSomeNoise ();
        }

        EditorGUILayout.Space ();
        serializedObject.ApplyModifiedProperties ();
    }
}
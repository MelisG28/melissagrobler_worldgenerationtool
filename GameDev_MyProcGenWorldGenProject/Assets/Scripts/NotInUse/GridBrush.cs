﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable, SerializeField]
public struct GridBrush
{
    public Sprite _brushSprite;
    public ColourPicker _brushColour;
}   

public enum ColourPicker
{
    Red,
    Blue,
    Green
}
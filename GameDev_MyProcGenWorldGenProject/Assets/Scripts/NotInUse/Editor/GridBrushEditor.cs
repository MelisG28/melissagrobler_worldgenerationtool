﻿// using UnityEditor;
// using UnityEngine;

// [CustomPropertyDrawer (typeof (GridBrushEditor))]
// public class GridBrushEditor : PropertyDrawer
// {
//     public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
//     {
//         EditorGUI.BeginProperty (position, label, property);

//         EditorGUILayout.BeginHorizontal (EditorStyles.helpBox);

//         SerializedProperty _itemSprite = property.FindPropertyRelative ("_brushSprite");
//         _itemSprite.objectReferenceValue = EditorGUILayout.ObjectField (_itemSprite.objectReferenceValue, typeof (Sprite), false, new GUILayoutOption[]
//         {
//             GUILayout.Width (20), GUILayout.Height (20)
//         });

//         SerializedProperty _brushColour = property.FindPropertyRelative ("_brushColour");
//         GUILayout.Label ("Brush Colour", EditorStyles.toolbarDropDown);
//         EditorGUILayout.PropertyField (_brushColour, GUIContent.none);

//         EditorGUILayout.EndHorizontal ();

//         EditorGUI.EndProperty ();
//     }
// }
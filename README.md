# MelissaGrobler_WorldGenerationTool

## [World Generation Tool for Unity](_PACKAGES/WorldGenerationTool.unitypackage)
This Unity package contains a simple procedural generation tool to make world creation easier. The package offers a 2D grid that can be drawn on to create your floor mesh as well as place different objects within your 3D world. 

### Using the package

- Import the package into a new Unity project
- Locate the object called "---Grid" in the scene hierarchy of the WorldProceduralGenerationScene scene.
- Specify the size of the grid you would like - base this on the size of your world.
- Click the "Generate Drawing Grid" Button and switch to Game View.
- Drag across or click on the grid blocks to change their colour - the colour will vary between red, blue, green and grey. Grey symbolizes empty space.
- Once you are happy with the floor mesh you have drawn on the grid, click the "Generate Floor Mesh" button. You can switch to Scene view to see the generate mesh. This mesh is a collection of quad meshes that have been joined together.
- Click the "Floor Mesh Completed" button to move onto the next step - placing objects in the world. By clicking this button, the grid will change to mimick the shape of the floor mesh that has been created.
- Click on grid blocks to change their colour - each colour represents a different prefab that will be spawned. Colours loop from grey to red to blue to green and then back to grey. Grey symbolizes empty space, colours represent objects.
- Once you are happy with the location of your objects, click the "Generate Prefabs" button to spawn your objects into the world.

### Buttons
- "Generate Drawing Grid": Generates the grid to draw on.
- "Generate Floor Mesh": Generates the custom floor mesh that has been drawn on the grid.
- "Floor Mesh Completed": Changes grid to the shape of the floor mesh and resets all the colours back to grey.
- "Generate Prefabs": Generates the prefabs specified in the inspector.
- "Clear Painted Cells": Resets all the colours of the grid blocks back to grey.
- "Delete Drawing Grid": Deletes the entire grid.
- "Delete Floor Mesh": Deletes the generated floor mesh.
- "Delete Prefabs": Deletes the generated prefabs.

### Perlin Noise
- Drag the PerlinNoise.cs script onto the object called Mesh in the hierarchy.
- Adjust the scale & power values.
- Click the "Add Noise" button to add Perlin Noise to your custom floor mesh.

### Software Used

- Unity 2019.4.8f1

### Tutorials Followed

- [Creating in Unity #5: Combining Meshes!](https://www.youtube.com/watch?v=wYAlky1aZn4)
- [Perlin Noise Plane Manipulation](https://answers.unity.com/questions/417793/perlin-noise-plane-manipulation.html)

### Project/Package Contents

- Scripts:
    - GridCell.cs
    - GridGen.cs
    - SelectChangeCells.cs
    - PerlinNoise.cs
    - Editor Scripts:
        - GridGenEditor.cs
        - PerlinNoiseEditor.cs
    - A folder name "NotInUse" contains scripts that weren't completed and thus not implemented

- Prefabs:
    - Cell.prefab
    - CubeBlue.prefab
    - CubeGreen.prefab
    - CubeRed.prefab
    - FloorMeshBlock.prefab

- Scenes:
    - WorldProceduralGenerationScene

- Sprites:
    - cell_Image.png
    - CircleImage.png
    - SquareImage.png
    - TriangleImage.png

- UnityAssetStorePackages:
    - 3 packages sourced from the Unity Asset Store for demonstration purposes of the procedural generation tool.
        - [LowPoly Rocks](https://assetstore.unity.com/packages/3d/environments/lowpoly-rocks-137970)
        - [Simple Houses Lite](https://assetstore.unity.com/packages/3d/environments/simple-houses-lite-78201)
        - [Low Poly Tree](https://assetstore.unity.com/packages/3d/low-poly-tree-62946)
